﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class Persona
    {

        // Atributos
        private String apellido;
        private String nombre;
        private DateTime fechaNacimiento;


        // Metodos, siempre en verbo
        public int calcularEdad(int anio)
        {
            // 2020 - 1993  = 27
            int anioFechaNacimiento = fechaNacimiento.Year;
            return anio - anioFechaNacimiento;
        }


      


        // Metodos Get and Set

        public String Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public DateTime FechaNacimiento
        {
            get { return fechaNacimiento; }
            set { fechaNacimiento = value; }
        }


    }
}
